# OpenML dataset: cmc

https://www.openml.org/d/45054

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is a subset of the 1987 National Indonesia Contraceptive Prevalence Survey. The samples are married women who were either not pregnant or do not know if they were at the time of interview. The problem is to predict the current contraceptive method choice (no use, long-term methods, or short-term methods) of a woman based on her demographic and socio-economic characteristics

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45054) of an [OpenML dataset](https://www.openml.org/d/45054). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45054/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45054/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45054/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

